extends Node2D

var screen_size

func _ready():
	screen_size = get_viewport_rect().size
	
func start_game():
	$hud.hide_elements()
	_position_bottom_horizontal_wall()
	_position_top_horizontal_wall()
	_position_left_vertical_wall()
	_position_right_vertical_wall()
	_position_paddle()
	_position_ai()
	_position_ball()
	
func _position_ball():
	$ball.start(Vector2(screen_size.x / 2, screen_size.y / 2))
	$ball.show()
	
func _position_bottom_horizontal_wall():
	$bottom_horizontal_wall.change_position(Vector2(1, screen_size.y))
	$bottom_horizontal_wall.change_scale(Vector2(screen_size.x - 1, screen_size.y))
	$bottom_horizontal_wall.show()
	
func _position_top_horizontal_wall():
	$top_horizontal_wall.change_position(Vector2(1, 0))
	$top_horizontal_wall.change_scale(Vector2(screen_size.x - 1, screen_size.y))
	$top_horizontal_wall.show()
	
func _position_left_vertical_wall():
	$left_vertical_wall.change_position(Vector2(1, 1))
	$left_vertical_wall.change_scale(Vector2(screen_size.x, screen_size.y))
	$left_vertical_wall.show()
	
func _position_right_vertical_wall():
	$right_vertical_wall.change_position(Vector2(screen_size.x, 1))
	$right_vertical_wall.change_scale(Vector2(screen_size.x, screen_size.y))
	$right_vertical_wall.show()
	
func _position_paddle():
	$paddle.start(Vector2(25, 25))
	$paddle.show()
	
func _position_ai():
	$ai.start(Vector2(screen_size.x - 25, screen_size.y - 150))
	$ai.show()

func _on_top_horizontal_wall_horizontal_wall_hit():
	$bounce.play()
	$ball.flip_direction_horizontal_wall()

func _on_bottom_horizontal_wall_horizontal_wall_hit():
	$bounce.play()
	$ball.flip_direction_horizontal_wall()
	
func _on_paddle_paddle_hit():
	$bounce.play()
	$ball.flip_direction_paddle()

func _on_left_vertical_wall_vertical_wall_hit():
	$ai_score.play()
	$hud.increase_ai_score()
	_position_ball()

func _on_right_vertical_wall_vertical_wall_hit():
	$player_score.play()
	$hud.increase_player_score()
	_position_ball()

func _on_ai_ai_hit():
	$bounce.play()
	$ball.flip_direction_paddle()
	
func _process(delta):
	$ai.change_position($ball.get_ball_position())

func _on_hud_start_game():
	start_game()
