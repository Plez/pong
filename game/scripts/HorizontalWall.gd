extends Area2D

var screen_size
signal horizontal_wall_hit

func _ready():
	screen_size = get_viewport_rect().size
	
func change_position(pos: Vector2):
	position.x = pos.x
	position.y = pos.y
	
func change_scale(new_scale: Vector2):
	var transform_scale = get_node("CollisionShape2D").get_shape()
	var old_scale = transform_scale.get_extents()
	transform_scale.set_extents(Vector2(old_scale.x + new_scale.x, old_scale.y))

func _on_HorizontalWall_body_entered(body):
	emit_signal("horizontal_wall_hit")
