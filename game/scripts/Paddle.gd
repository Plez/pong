extends Area2D

export var speed := 500
var velocity = Vector2(0, 1)
var screen_size
var position_to_reach = Vector2(0, 1)
signal paddle_hit

func _ready():
	set_process_input(true)
	screen_size = get_viewport_rect().size
	
func start(pos: Vector2):
	position = pos
	show()
	velocity = velocity.normalized() * speed
	$AnimatedSprite.play()
	
func _process(delta):
	if Input.is_action_pressed("ui_up"):
		velocity.y = -1
		position += velocity * delta * speed
	elif Input.is_action_pressed("ui_down"):
		velocity.y = 1
		position += velocity * delta * speed
	else:
		if position.y < position_to_reach.y:
			position += velocity * delta
		if position.y > position_to_reach.y:
			position -= velocity * delta
	position.y = clamp(position.y, 110, screen_size.y - 110)

func _on_Paddle_body_entered(body):
	emit_signal("paddle_hit")
	
func _on_Paddle_input_event(viewport, event, shape_idx):
	if event is InputEventScreenDrag:
		position_to_reach = event.position
