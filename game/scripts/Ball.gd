extends KinematicBody2D

export var speed := 800
var velocity = Vector2()
var screen_size
signal changed_direction

func _ready():
	screen_size = get_viewport_rect().size
	
func start(pos: Vector2):
	position = pos
	show()
	randomize()
	var x_component = rand_range(0.5, 1.0)
	var x_signal = randi() % 2;
	if x_signal == 0:
		x_component = -1 * x_component
	var y_component = rand_range(0.5, 1.0)
	var y_signal = randi() % 2;
	if y_signal == 0:
		y_component = -1 * y_component
	velocity.x = x_component
	velocity.y = y_component
	velocity = velocity.normalized() * speed
	$AnimatedSprite.play()
	
func flip_direction_horizontal_wall():
	velocity.y = -velocity.y
	
func flip_direction_vertical_wall():
	$CollisionShape2D.set_deferred("disabled", true)
	velocity.x = 0
	velocity.y = 0
	$AnimatedSprite.stop()
	
func flip_direction_paddle():
	velocity.x = -velocity.x
	
func get_ball_position() -> Vector2:
	return position
	
func _process(delta):
	position += velocity * delta
	position.x = clamp(position.x, 0, screen_size.x)
	position.y = clamp(position.y, 0, screen_size.y)
#	pass
