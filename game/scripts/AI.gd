extends Area2D

export var speed := 200
var velocity = Vector2(0, 1)
var screen_size
var position_to_reach
signal ai_hit

func _ready():
	screen_size = get_viewport_rect().size
	
func start(pos: Vector2):
	position = pos
	show()
	velocity = velocity.normalized() * speed
	$AnimatedSprite.play()
	
func change_position(pos: Vector2):
	position_to_reach = pos
	
func _process(delta):
	if position.y < position_to_reach.y:
		position += velocity * delta
	if position.y > position_to_reach.y:
		position -= velocity * delta
	position.y = clamp(position.y, 110, screen_size.y - 110)

func _on_AI_body_entered(body):
	emit_signal("ai_hit")
