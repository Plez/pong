extends CanvasLayer

signal start_game
var player_score := 0
var ai_score := 0

func _ready():
	$PlayerScore.text = String(player_score)
	$AIScore.text = String(ai_score)

func increase_player_score():
	player_score += 1
	$PlayerScore.text = String(player_score)
	
func increase_ai_score():
	ai_score += 1
	$AIScore.text = String(ai_score)

func _on_StartButton_pressed():
	emit_signal("start_game")
	$PlayerScore.show()
	$AIScore.show()
	pass # Replace with function body.

func hide_elements():
	$StartButton.hide()